#include <ModbusMaster.h>               //Library for using ModbusMaster
#include <LiquidCrystal.h>              //Library for using LCD display
#include <SoftwareSerial.h>

#define MAX485_DE      3
#define MAX485_RE_NEG  2

ModbusMaster node;                    //object node for class ModbusMaster
SoftwareSerial serial1(8, 9);

void preTransmission()            //Function for setting stste of Pins DE & RE of RS-485
{
  digitalWrite(MAX485_RE_NEG, 1);             
  digitalWrite(MAX485_DE, 1);
}

void postTransmission()
{
  digitalWrite(MAX485_RE_NEG, 0);
  digitalWrite(MAX485_DE, 0);
}

void setup()
{
  
  pinMode(MAX485_RE_NEG, OUTPUT);
  pinMode(MAX485_DE, OUTPUT);
  
  pinMode(4,INPUT);
  pinMode(5,INPUT);
  
  digitalWrite(MAX485_RE_NEG, 0);
  digitalWrite(MAX485_DE, 0);

  Serial.begin(9600);             //Baud Rate as 115200
  serial1.begin(9600);            //Baud Rate as 115200

  //node.begin(1, Serial);            //Slave ID as 1
  node.begin(1,serial1);
  node.preTransmission(preTransmission);         //Callback for configuring RS-485 Transreceiver correctly
  node.postTransmission(postTransmission);
}

void loop()
{
    uint16_t value = 0;
    uint8_t stat = 1;
  

    stat = node.readInputRegisters(0x30000,2);
    if(stat == 0)
    {
    value = node.getResponseBuffer(1);
    Serial.print("\r\nValue => ");
    Serial.print(value,HEX);
    }
    delay(500);



//    node.writeSingleRegister(0x40001,1);               //Writes 1 to 0x40001 holding register
//    node.writeSingleRegister(0x40001,0);              //Writes 0 to 0x40001 holding register
//
//    node.writeSingleRegister(0x40002,1);              //Writes 1 to 0x40002 holding register
//    node.writeSingleRegister(0x40002,0);                //Writes 0 to 0x40002 holding register

 
}
