#include <ModbusMaster.h>               //Library for using ModbusMaster
#include <LiquidCrystal.h>              //Library for using LCD display
#include <SoftwareSerial.h>

#define MAX485_DE      3
#define MAX485_RE_NEG  2

ModbusMaster node;                    //object node for class ModbusMaster
SoftwareSerial serial1(8, 9);

//LiquidCrystal lcd(8,9,10,11,12,13);  //Object lcd for class Liquidcrystal with LCD pins (RS, E, D4, D5, D6, D7) that are connected with Arduino UNO.
//uint8_t RespAry[20];
//uint8_t Expt_Resp[8] = {0x01,0x06,0x27,0x0F,0x00,0x01,0x72,0xBD};
//uint8_t Start_Frame[8] = {0x01,0x03,0x00,0x00,0x00,0x0A,0xC5,0xCD}; //{0x01,0x06,0x27,0x0F,0x00,0x01,0x72,0xBD}; 
//uint8_t Req_Frame[20] = {0x01,0x06,0x27,0x0F,0x00,0x02,0x32,0xBC};
//int Heartbeat = ;
//int Req_Data =;
void preTransmission()            //Function for setting stste of Pins DE & RE of RS-485
{
  digitalWrite(MAX485_RE_NEG, 1);             
  digitalWrite(MAX485_DE, 1);
}

void postTransmission()
{
  digitalWrite(MAX485_RE_NEG, 0);
  digitalWrite(MAX485_DE, 0);
}

void setup()
{
  
  pinMode(MAX485_RE_NEG, OUTPUT);
  pinMode(MAX485_DE, OUTPUT);
  
  pinMode(4,INPUT);
  pinMode(5,INPUT);
  
  digitalWrite(MAX485_RE_NEG, 0);
  digitalWrite(MAX485_DE, 0);

  Serial.begin(9600);             //Baud Rate as 115200
  serial1.begin(9600);            //Baud Rate as 115200

  //node.begin(1, Serial);            //Slave ID as 1
  node.begin(1,serial1);
  node.preTransmission(preTransmission);         //Callback for configuring RS-485 Transreceiver correctly
  node.postTransmission(postTransmission);
}

void loop()
{
  //uint8_t i;
//  float value = analogRead(A0);
    uint16_t value = 0;
    uint8_t stat = 1;
  
 // Serial.println(F("Sending Start Byte"));
//  for(i = 0; i < 8; i++)
//   {
//     Serial.print(Start_Frame[i]);
//     mySerial.write(Start_Frame[i]);
//   }
//
//node.writeSingleRegister(0x40000,value); 
   
//    node.writeSingleRegister(0x40001,1);               //Writes 1 to 0x40001 holding register
//    if(HeartBeat == 0)
//    {
//    stat = node.readHoldingRegisters(0x40001,2);
    stat = node.readInputRegisters(0x30000,2);
    if(stat == 0)
    {
    value = node.getResponseBuffer(1);
    Serial.print("\r\nValue => ");
    Serial.print(value,HEX);
    }
    delay(500);
//    
    //HeartBeat = 1;
    //}
    
////   for(i = 0; i < 8; i++)
//   {
////     node.writeSingleRegister(0x40000,Start_Frame[i]);        //Writes value to 0x40000 holding register
//      node.writeSingleRegister(0x40001,Start_Frame[i]);
//      Serial.print(Start_Frame[i]);
//   }
//   delay(1000);
//  //Serial.println(value);
//    response = node.readHoldingRegisters(0x40001,1);
//    delay(100);
//    if(Response == ExptResponse)
//    {
//      Serial.println(F("Connection established"));
//    }
//    else
//    {
//     
//    }
//    Serial.println(F("Connection established"));
//    node.writeSingleRegister(0x40001,1);
    
    

//   if(mySerial.available() > 0)
//  {
//    for(i = 0; i < 8; i++)
//    {
//      RespAry[i] = mySerial.read();
//    }
//  }
//  int a= digitalRead(4);                           //Reads state of push button 
//  int b= digitalRead(5);
//   if(RespAry[8] == Expt_Resp[8])
//   {
//    Serial.println(F("Connection establishing...."));
//   }
//   delay(100);
//   Serial.println(F("Connection established"));
//   delay(1000);
//   Serial.println(F("Sending Request For Pressure Sensor"));
//    for(i=0; i<8; i++)
//   {
//     Serial.print(Req_Frame[i]);
//     mySerial.write(Req_Frame[i]);
//   }


//    node.writeSingleRegister(0x40001,1);               //Writes 1 to 0x40001 holding register
//    node.writeSingleRegister(0x40001,0);              //Writes 0 to 0x40001 holding register
//
//    node.writeSingleRegister(0x40002,1);              //Writes 1 to 0x40002 holding register
//    node.writeSingleRegister(0x40002,0);                //Writes 0 to 0x40002 holding register

 
}
