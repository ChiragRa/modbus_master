

#include <SoftwareSerial.h> //is necesary for the library!! 
//#include <PubSubClient.h>

SoftwareSerial GSMport(A1, A2);  //(RX,TX)to declare the library


#define dbg

char SIM_APN[] = {"INTERNET"}; // for IDEA Network
//char SIM_APN[] = {"WWW"}; // for VODAFONE Network
char SIM_UserName[] = {""};
char SIM_Pass[] = {""};

// MQTT Initializations

unsigned int Counter = 20; // changed to unsign to int
//float Counter = 9999990.89;
char str1[10] = {"999"};
unsigned long datalength, checksum, rLength;
unsigned short topiclength;
unsigned short topiclength2;
unsigned char topic[30];
//String topic;
//String Counter1;
//String SMQTT_Data;

char str[250];
unsigned char encodedByte;
int X;

unsigned short MQTTProtocolNameLength;
unsigned short MQTTClientIDLength;
unsigned short MQTTUsernameLength;
unsigned short MQTTPasswordLength;
unsigned short MQTTTopicLength;

const char * MQTTHost = "io.adafruit.com";
const char * MQTTPort = "1883";        // checked, also 8883
const char * MQTTClientID = "ABCDEF";
const char * MQTTTopic = "ChiragRathod/feeds/data";
const char * MQTTTopic2 = "ChiragRathod/feeds/onoff";
const char * MQTTProtocolName = "MQTT";
const char MQTTLVL = 0x03;
const char MQTTFlags = 0xC2;
const unsigned int MQTTKeepAlive = 60; // testing
//const char MQTTKeepAlive = 0x3C;

const char * MQTTUsername = "ChiragRathod";
const char * MQTTPassword = "ec4798b043174669a98465a3c4e62be5";
const char MQTTQOS = 0x00;
const char MQTTPacketID = 0x0001;

char sim800lreset = 4;

void setup() 
{
  // put your setup code here, to run once:
Serial.begin(9600);
GSMport.begin(9600);

pinMode(sim800lreset, OUTPUT);
delay(3000);
MQTTProtocolNameLength = strlen(MQTTProtocolName);

//sendATcommand((char *)"AT", (char*)"OK", 2000);

//sendATcommand((char *)"AT+CSQ", (char*)"OK", 2000);


}

void loop() 

{
 
  //resetModem();
 // Serial.println("Test");
  if(initTCP()) {
    MQTTConnect();
    readServerResponse("AT+CIPRXGET=2,1024", "OK", "ERROR", 10000);//receive data from n/w

   // MQTTsubscribe();
    //readServerResponse("AT+CIPRXGET=2,1024", "OK", "ERROR", 10000);//receive data from n/w

    while (1) {
      MQTTpublish();
      //readServerResponse("AT+CIPRXGET=2,1024", "OK", "ERROR", 10000);

//      MQTTsubscribe();
//      readServerResponse("AT+CIPRXGET=2,1024", "OK", "ERROR", 10000);//receive data from n/w

      Counter++;
      delay(5000);
    }
  }
  else
    sendATcommand2("AT+CIPSHUT", "OK", "ERROR", 1000);

}

int  MQTTConnect() {

  if (sendATcommand2("AT+CIPSEND", ">", "ERROR", 1000)) {
       
    GSMport.write(0x10);
    MQTTProtocolNameLength = strlen(MQTTProtocolName);
    MQTTClientIDLength = strlen(MQTTClientID);
    MQTTUsernameLength = strlen(MQTTUsername);
    MQTTPasswordLength = strlen(MQTTPassword);
    datalength = MQTTProtocolNameLength + 2 + 4 + MQTTClientIDLength + 2 + MQTTUsernameLength + 2 + MQTTPasswordLength + 2;
    X = datalength;
    do
    {
      encodedByte = X % 128;
      X = X / 128;
      // if there are more data to encode, set the top bit of this byte
      if ( X > 0 ) {
        encodedByte |= 128;
      }

      GSMport.write(encodedByte);
    }
    while ( X > 0 );
    GSMport.write(MQTTProtocolNameLength >> 8);
    GSMport.write(MQTTProtocolNameLength & 0xFF);
    GSMport.write(MQTTProtocolName);

    GSMport.write(MQTTLVL); // LVL
    GSMport.write(MQTTFlags); // Flags
    GSMport.print(MQTTKeepAlive >> 8);      // write(unsigned int) is ambiguous so byte inorder to determine as integer
    GSMport.write(MQTTKeepAlive & 0xFF);


    GSMport.write(MQTTClientIDLength >> 8);
    GSMport.write(MQTTClientIDLength & 0xFF);
    GSMport.print(MQTTClientID);


    GSMport.write(MQTTUsernameLength >> 8);
    GSMport.write(MQTTUsernameLength & 0xFF);
    GSMport.print(MQTTUsername);


    GSMport.write(MQTTPasswordLength >> 8);
    GSMport.write(MQTTPasswordLength & 0xFF);
    GSMport.print(MQTTPassword);

    GSMport.write(0x1A);
    if (sendATcommand2("", "SEND OK", "SEND FAIL", 5000)) {
      Serial.println(F("CONNECT PACKET SUCCESS"));
      return 1;
    }
    else return 0;
  }
}
char test[10];

int  MQTTpublish() {
  if (sendATcommand2("AT+CIPSEND", ">", "ERROR", 1000)) {

    memset(str, 0, sizeof(str));

    topiclength = sprintf((char*)topic, MQTTTopic);
    sprintf((char*)test, "%u.", Counter);
    datalength = sprintf((char*)str, "%s%s", topic, test);//"Counter$$%%^&&&&**&^%$#");
    Serial.println(str);
    delay(1000);
    GSMport.write(0x30);
    X = datalength + 2;
    do
    {
      encodedByte = X % 128;
      X = X / 128;
      // if there are more data to encode, set the top bit of this byte
      if ( X > 0 ) {
        encodedByte |= 128;
      }
      GSMport.write(encodedByte);
    }
    while ( X > 0 );

    GSMport.write(topiclength >> 8);
    GSMport.write(topiclength & 0xFF);
    GSMport.print(str);
    GSMport.write(0x1A);
    if (sendATcommand2("", "SEND OK", "SEND FAIL", 5000)) {
      Serial.println(F("PUBLISH PACKET SENT"));
      return 1;
    }
    else return 0;
  }
}

//void MQTTsubscribe() {
//
//  if (sendATcommand2("AT+CIPSEND", ">", "ERROR", 1000)) {
//
//    memset(str, 0, 250);
//    topiclength2 = strlen(MQTTTopic2);
//    datalength = 2 + 2 + topiclength2 + 1;
//    delay(1000);
//
//    GSMport.write(0x82);
//    X = datalength;
//    do
//    {
//      encodedByte = X % 128;
//      X = X / 128;
//      // if there are more data to encode, set the top bit of this byte
//      if ( X > 0 ) {
//        encodedByte |= 128;
//      }
//      GSMport.write(encodedByte);
//    }
//    while ( X > 0 );
//   // GSMport.write((byte)MQTTPacketID >> 8);   // write(int) is ambiguous so byte inorder to determine as integer
//   GSMport.print(MQTTPacketID >> 8);        //to send the characters representing digits of a no
//   
//    GSMport.write(MQTTPacketID & 0xFF);
//    GSMport.write(topiclength2 >> 8);
//    GSMport.write(topiclength2 & 0xFF);
//    GSMport.print(MQTTTopic2);
//    GSMport.write(MQTTQOS);
//
//    GSMport.write(0x1A);
//    if (sendATcommand2("", "SEND OK", "SEND FAIL", 5000)) {
//      Serial.println(F("SUBSCRIBE PACKET SENT"));
//      return 1;
//    }
//    else return 0;
//  }
//
//}


int8_t readServerResponse(char* ATcommand, char* expected_answer1, char* expected_answer2, unsigned int timeout) {
  unsigned long nowMillis = millis();
  GSMport.println(ATcommand);
  delay(3000);
 
  if (GSMport.available()) {
    while (char(Serial.read()) != 0x24) {
      if ((millis() - nowMillis) > 2000) {
        Serial.println("NO DATA RECEIVED FROM REMOTE");
        break;
      }
    }
    nowMillis=(millis());
    while (GSMport.available()) {
      Serial.print(char(GSMport.read()));
    }
  }

}

boolean resetModem() {
  uint8_t answer = 0;
  digitalWrite(sim800lreset, LOW);
  delay(1000);
  digitalWrite(sim800lreset, HIGH);
  // checks if the module is started
  delay(3000);
  answer = sendATcommand("AT", "OK", 2000);
  if (answer == 1)
    sendATcommand("AT", "OK", 2000); // turn off the echo
  else if (answer == 0)
  {
    // power on pulse
    digitalWrite(sim800lreset, LOW);
    delay(1000);
    digitalWrite(sim800lreset, HIGH);

    // waits for an answer from the module
    int trials = 0;
    while (answer == 0) 
    {
      trials++;
      // Send AT every two seconds and wait for the answer
      answer = sendATcommand("AT", "OK", 2000);
      if (trials == 5) 
      {
        Serial.println(F("Gsm Start Fail"));
        //break;
        return false;
      }
    }
    sendATcommand("ATE0", "OK", 2000);// turn off the echo
  }
  else if (answer == 1)
  return true;
}

int8_t sendATcommand(char* ATcommand, char* expected_answer, unsigned int timeout) {                          

  uint8_t x = 0,  answer = 0;
  char response[500];
  unsigned long previous;
  char* str;
  uint8_t index = 0;

  memset(response, '\0', 100);    // Initialize the string

  delay(100);

  while ( GSMport.available() > 0) GSMport.read();   // Clean the input buffer

  GSMport.println(ATcommand);    // Send the AT command
#ifdef dbg
  Serial.println(ATcommand);    // Send the AT command
#endif


  x = 0;
  previous = millis();

  // this loop waits for the answer
  do {
    if (GSMport.available() != 0) 
    {
      // if there are data in the UART input buffer, reads it and checks for the asnwer
      response[x] = GSMport.read();
      //GSMport.print(response[x]);
      x++;
      // check if the desired answer  is in the response of the module
      if (strstr(response, expected_answer) != NULL)
      {
        answer = 1;

      }
    }
  }
  // Waits for the asnwer with time out
  while ((answer == 0) && ((millis() - previous) < timeout));

#ifdef dbg
  Serial.println(response);    // Send the AT command
#endif
  return answer;
}





int8_t sendATcommand2(char* ATcommand, char* expected_answer1, char* expected_answer2, unsigned int timeout) {

  uint8_t x = 0,  answer = 0;
  char response[100];
  unsigned long previous;

  memset(response, '\0', 100);    // Initialize the string

  delay(100);

  GSMport.flush();
  GSMport.println(ATcommand);    // Send the AT command
  //if(strstr(ATcommand, "AT+CIPSEND")!=NULL) GSMport.write(0x1A);

 #ifdef dbg
 Serial.println(ATcommand);    // Send the AT command
#endif

  x = 0;
  previous = millis();

  // this loop waits for the answer
  do {
    // if there are data in the UART input buffer, reads it and checks for the asnwer
    if (GSMport.available() != 0) 
    {
      response[x] = GSMport.read();
      x++;
      // check if the desired answer 1  is in the response of the module
      if (strstr(response, expected_answer1) != NULL)
      {
        answer = 1;
        while (Serial.available()) 
        {
          response[x] = GSMport.read();
          x++;
        }
      }
      // check if the desired answer 2 is in the response of the module
      else if (strstr(response, expected_answer2) != NULL)
      {
        answer = 2;
        while (Serial.available()) {
          response[x] = GSMport.read();
          x++;
        }
      }

    }
  }
  // Waits for the asnwer with time out
  while ((answer == 0) && ((millis() - previous) < timeout));
#ifdef dbg
  Serial.println(response);
#endif
  return answer;
}
//
//
//
//
//
//
//
int initTCP() {

char *aux_str;
  
  resetModem();
  sendATcommand2("ATE0", "OK", "ERROR", 2000);
  if(sendATcommand2("ATE0", "OK", "ERROR", 2000) == 1)
    Serial.println("OKK");
    else
    Serial.println("ERRORR");
    

  delay(2000);


  Serial.println(F("Connecting to the network..."));

  sendATcommand2("AT+CSQ", "OK", "ERROR", 1000);
  
  while ( sendATcommand2("AT+CREG?", "+CREG: 0,1", "+CREG: 0,5", 1000) == 0 );
  delay(2000);

  
  
  if (sendATcommand2("AT+CIPMUX=0", "OK", "ERROR", 1000) == 1)      // Selects Single-connection mode
  {
    sendATcommand2("AT+CIPMODE?", "OK", "ERROR", 1000);
    
//   if (sendATcommand2("AT+CIPRXGET=2", "OK", "ERROR", 1000) == 1) { //RECEIVE DATA manually FROM THE REMOTE SERVER
//     int8_t answer = 0;
//     delay(1000);
//     if (!(sendATcommand2("AT+CIPMODE=0", "OK", "ERROR", 1000) ))return 0;//srt non transparent mode for data sending
//      delay(500);
//      if (!(sendATcommand2("AT+CIPSRIP=0", "OK", "ERROR", 1000) ))return 0;//Do not show the prompt during receiving data from server
//      delay(500);
      while (sendATcommand("AT+CGATT?", "+CGATT: 1", 5000) == 0 );
      delay(1000);

      // Waits for status IP INITIAL
      while (sendATcommand("AT+CIPSTATUS", "INITIAL", 5000) == 0);
      delay(1000);

//      snprintf(aux_str, sizeof(aux_str), "AT+CSTT=\"%s\",\"%s\",\"%s\"", SIM_APN, SIM_UserName , SIM_Pass  );

//Serial.println(aux_str);
      // Sets the APN, user name and password
//      if (sendATcommand2(aux_str, "OK",  "ERROR", 30000) == 1)
//AT+CSTT="INTERNET","",""
      if (sendATcommand2("AT+CSTT=\"WWW\",\"\",\"\"","OK", "ERROR", 1000) == 1)
      {

        // Waits for status IP START
        if (sendATcommand("AT+CIPSTATUS", "START", 500)  == 0 )
          delay(3000);

        // Brings Up Wireless Connection
        if (sendATcommand2("AT+CIICR", "OK", "ERROR", 30000) == 1)
        {

          // Waits for status IP GPRSACT
          while (sendATcommand("AT+CIPSTATUS", "GPRSACT", 500)  == 0 );
          delay(3000);

          // Gets Local IP Address
          if (sendATcommand2("AT+CIFSR", ".", "ERROR", 10000) == 1)
          {

            // Waits for status IP STATUS
            while (sendATcommand("AT+CIPSTATUS", "IP STATUS", 500)  == 0 );
            //delay(5000);
            delay(5000);

            Serial.println(F("Opening TCP"));
            //snprintf(aux_str, sizeof(aux_str), "AT+CIPSTART=\"TCP\",\"%s\",\"%s\"", MQTTHost, MQTTPort);
//AT+CIPSTART="TCP","exploreembedded.com",80

            // Opens a TCP socket
//            if (sendATcommand2(aux_str, "OK\r\n\r\nCONNECT", "CONNECT FAIL", 30000) == 1)
           // if (sendATcommand2("AT+CIPSTART=\"TCP\",\"exploreembedded.com\",80", "OK\r\n\r\nCONNECT", "CONNECT FAIL", 30000) == 1)
           if (sendATcommand2("AT+CIPSTART=\"TCP\",\"io.adafruit.com\",1883", "OK\r\n\r\nCONNECT", "CONNECT FAIL", 30000) == 1)
            {

              Serial.println(F("Connected"));
              return 1;

            }

            else
            {
              Serial.println(F("Error opening the connection"));
              Serial.println(F("UNABLE TO CONNECT TO SERVER "));
              return 0;

            }

          }
          else
          {
            Serial.println(F("ERROR GETTING IP ADDRESS "));
            return 0;

          }
        }
        else
        {
          Serial.println(F("ERROR BRINGING UP WIRELESS CONNECTION"));
          return 0;
        }
      }
      else {
        Serial.println(F("Error setting the APN"));
        return 0;
      }


    }
    else
    {
      Serial.println(F("Error setting CIPRXGET"));
      return 0;
    }


//  }
}
