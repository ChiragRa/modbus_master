//Pin Details
//
//  Serial RX A1        // Input
//  Serial TX A2        // Output

#include <ModbusMaster.h>               //Library for using ModbusMaster
#include <SoftwareSerial.h>
#include "MQTTLibry.h"
#include "GSMLibry.h"

#define MAX485_DE      3
#define MAX485_RE_NEG  2

ModbusMaster node;                    //object node for class ModbusMaster
SoftwareSerial serial1(8, 9);
uint32_t CurTime;

void preTransmission()            //Function for setting stste of Pins DE & RE of RS-485
{
  digitalWrite(MAX485_RE_NEG, 1);             
  digitalWrite(MAX485_DE, 1);
}

void postTransmission()
{
  digitalWrite(MAX485_RE_NEG, 0);
  digitalWrite(MAX485_DE, 0);
}

void setup()
{
  
  pinMode(MAX485_RE_NEG, OUTPUT);
  pinMode(MAX485_DE, OUTPUT);
  
  pinMode(4,INPUT);
  pinMode(5,INPUT);
  
  digitalWrite(MAX485_RE_NEG, 0);
  digitalWrite(MAX485_DE, 0);
  
  InitGSM();
  Serial.println(F("GSM Initialize"));
  delay(100);
  
  InitMQTT();
  Serial.println(F("MQTT Initialize"));
  delay(100);  

  Serial.begin(9600);             //Baud Rate as 115200
  serial1.begin(9600);            //Baud Rate as 115200

  //node.begin(1, Serial);            //Slave ID as 1
  node.begin(1,serial1);
  node.preTransmission(preTransmission);         //Callback for configuring RS-485 Transreceiver correctly
  node.postTransmission(postTransmission);
}

void loop()
{
    uint16_t value = 0;
    uint8_t stat = 1;
  

    stat = node.readInputRegisters(0x30000,2);
    if(stat == 0)
    {
    value = node.getResponseBuffer(1);
    Serial.print("\r\nValue => ");
    Serial.print(value,HEX);
    }
    delay(500);
    
    InitTCP();
    
    if(bTCPConnected == 1)
  {
    if(BIP_State == _IP_CONNECT)
    {
      if(bMQTTConct == 0)
      {
        bMQTTConct = SendMQTTPacket(_MQTT_CON);
        SendATCommand(AT_CIPRXGET2, 2000);

        if(bMQTTConct == 1)
        {
          CurTime = millis();
        }
      }
      else if(bMQTTConct == 1)
      {
        
        SendMQTTPacket(_MQTT_PUB_01);
      }
    }
  }
    
//    InitTCP();
//
//    SendMQTTPacket(_MQTT_CON);
//    SendATCommand(AT_CIPRXGET2, 2000);
//    delay(500);

//    SendMQTTPacket(_MQTT_PUB_01);

}
